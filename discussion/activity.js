// Setup
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

// For reading json data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// MongoDB connection
mongoose.connect(
  "mongodb+srv://twapegg:mEDXlo13wjUGhSal@wdc028-course-booking.5akuxcl.mongodb.net/b281_auth?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

const db = mongoose.connection;

// Connection checking
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB database.");
});

// 1. Create a User schema
const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

// 2. Create a User model
const User = mongoose.model("User", userSchema);

// 3. Create a POST route that will access the "/signup" route that will create a user
app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }).then((result) => {
    // Check for existing user
    if (result !== null && result.username === req.body.username) {
      return res.send("User already exists!");
    } else {
      // Create a new user
      const user = new User({
        username: req.body.username,
      });
      user.save().then((result) => {
        res.send(result);
      });
    }
  });
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
