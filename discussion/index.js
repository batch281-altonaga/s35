// Data Persistence via Mongoose ODM
const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

// Allows the app to read json data
// Allows the app to read data forms
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// MongoDB connection
// Connecting to the database
mongoose.connect(
  "mongodb+srv://twapegg:mEDXlo13wjUGhSal@wdc028-course-booking.5akuxcl.mongodb.net/b281_to-do?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Set notification for connection success or failure
const db = mongoose.connection;

// If a connection error occurs, log the error message
db.on("error", console.error.bind(console, "connection error:"));

// If a connection is successful, log a success message
db.once("open", () => {
  console.log("Connected to MongoDB database.");
});

// Mongoose Schemas
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

// Models [Task models]
const Task = mongoose.model("Task", taskSchema);

// Creation of todo list routes

// Creating a new task
app.post("/tasks", (req, res) => {
  // Check if document already exists
  Task.findOne({ name: req.body.name }).then((result) => {
    if (result !== null && result.name === req.body.name) {
      return res.send("Duplicate task found!");
    } else {
      // Else, create a new task
      const task = new Task({
        name: req.body.name,
      });
      // Save the task
      task.save().then((result) => {
        res.send(result);
      });
    }
  });
});

// Get all the tasks
app.get("/tasks", (req, res) => {
  Task.find().then((result, err) => {
    // Catch errors
    if (err) {
      return console.log(err);
    } else {
      res.send(result);
    }
  });
});

// Listen to the port
app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
